### Flipping Pocket Adventures
[Website](https://flipping-pocket-adventures.codeberg.page/)

A rules light Roleplaying System that fits in your Wallet.

This is the repository for the core rules. The SVG files of the Rules are edited with Inkscape.

The fonts used are:
https://www.fontsquirrel.com/fonts/bitter
https://www.fontsquirrel.com/fonts/andika-basic

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
